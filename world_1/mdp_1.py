import numpy as np 
import mdptoolbox
import mdptoolbox.util
import seaborn as sns
import matplotlib.pyplot as plt
from helpers import grid_world_example
from helpers import visualizePolicy
from scipy import linalg
import time
from mdp_class import QLearning


obstacles = [
    (1, 1), (1, 2), (1, 3), (1, 4), (1, 5), 
    (2, 5), 
    (3, 2), (3, 5), 
    (4, 2), 
    (5, 0), (5, 1), (5, 2), (5, 4), (5, 5), (5, 6), 
    (6, 4), 
    (7, 4)
]

walls = np.zeros((8, 8))
for obstacle in obstacles:
    walls[obstacle[0], obstacle[1]] = 1

P, R = grid_world_example(grid_size=(8, 8), black_cells=obstacles, green_cell_loc=(0, 7), red_cell_loc=(2, 4), start_loc=(7, 0))
mdptoolbox.util.check(P, R)

vi = mdptoolbox.mdp.ValueIteration(P, R, 0.99)
vi.run()
visualizePolicy(vi, (8, 8), walls, start_loc=(7, 0), end_loc=(0, 7), trap_loc=(2, 4), name="correct_policy.png", approach="perfect")
correct_policy = np.reshape(vi.policy, (8, 8))

vi = mdptoolbox.mdp.ValueIteration(P, R, 0.65)
vi.run()
visualizePolicy(vi, (8, 8), walls, start_loc=(7, 0), end_loc=(0, 7), trap_loc=(2, 4), name="vi_policy.png", approach="vi")

pi = mdptoolbox.mdp.PolicyIteration(P, R, 0.45)
pi.run()
visualizePolicy(pi, (8, 8), walls, start_loc=(7, 0), end_loc=(0, 7), trap_loc=(2, 4), name="pi_policy.png", approach="pi")

np.random.seed(0)
ql = mdptoolbox.mdp.QLearning(P, R, 0.99, 100000)
ql.run()
visualizePolicy(ql, (8, 8), walls, start_loc=(7, 0), end_loc=(0, 7), trap_loc=(2, 4), name="ql_policy.png", approach="ql")

iters_vi = []
accuracy_vi = []
times_vi = []
converged = 1
for rate in np.linspace(0.1, 0.99, 90):
    vi = mdptoolbox.mdp.ValueIteration(P, R, discount=rate)
    start = time.time()
    vi.run()
    times_vi.append(time.time() - start)
    iters_vi.append(vi.iter)
    acc = (np.reshape(vi.policy, (8, 8)) == correct_policy).sum() / 64
    accuracy_vi.append(acc)
    if np.array_equal(np.reshape(vi.policy, (8, 8)), correct_policy) and rate < converged:
        converged = rate


iters_pi = []
accuracy_pi = []
times_pi = []
converged = 1
for rate in np.linspace(0.1, 0.99, 90):
    pi = mdptoolbox.mdp.PolicyIteration(P, R, discount=rate, eval_type=1)
    start = time.time()
    pi.run()
    times_pi.append(time.time() - start)
    iters_pi.append(pi.iter)
    acc = (np.reshape(pi.policy, (8, 8)) == correct_policy).sum() / 64
    accuracy_pi.append(acc)
    if np.array_equal(np.reshape(pi.policy, (8, 8)), correct_policy) and rate < converged:
        converged = rate

plt.plot(np.linspace(0.1, 0.99, 90), iters_vi, label="Value Iteration")
plt.plot(np.linspace(0.1, 0.99, 90), iters_pi, label="Policy Iteration")
plt.legend(loc="best")
plt.xticks(np.arange(0.1, 1.1, 0.1))
#plt.axvline(x=converged, linestyle="--", color="black")
plt.grid()
plt.title("8x8 Grid")
plt.xlabel("Discount Rate")
plt.ylabel("Iterations")
plt.savefig("iterations.png")
plt.close()
plt.plot(np.linspace(0.1, 0.99, 90), accuracy_vi, label="Value Iteration")
plt.plot(np.linspace(0.1, 0.99, 90), accuracy_pi, label="Policy Iteration")
plt.legend(loc="best")
plt.xticks(np.arange(0.1, 1.1, 0.1))
plt.grid()
plt.title("8x8 Grid")
plt.xlabel("Discount Rate")
plt.ylabel("Policy Correctness")
plt.savefig("accuracy.png")
plt.close()
plt.plot(np.linspace(0.1, 0.99, 90), times_vi, label="Value Iteration")
plt.plot(np.linspace(0.1, 0.99, 90), times_pi, label="Policy Iteration")
plt.legend(loc="best")
plt.xticks(np.arange(0.1, 1.1, 0.1))
plt.grid()
plt.title("8x8 Grid")
plt.xlabel("Discount Rate")
plt.ylabel("Time (s)")
plt.savefig("time.png")
plt.close()


accuracy = []
times = []
converged = 1
for rate in np.linspace(0.1, 0.99, 90):
    ql = QLearning(P, R, discount=rate, n_iter=100000)
    start = time.time()
    ql.run()
    times.append(time.time() - start)
    acc = (np.reshape(ql.policy, (8, 8)) == correct_policy).sum() / 64
    accuracy.append(acc)
    if np.array_equal(np.reshape(ql.policy, (8, 8)), correct_policy) and rate < converged:
        converged = rate

plt.plot(np.linspace(0.1, 0.99, 90), accuracy)
plt.xticks(np.arange(0.1, 1.1, 0.1))
plt.grid()
plt.title("8x8 Grid")
plt.xlabel("Discount Rate")
plt.ylabel("Policy Correctness")
plt.savefig("ql_accuracy.png")
plt.close()
plt.plot(np.linspace(0.1, 0.99, 90), times)
plt.xticks(np.arange(0.1, 1.1, 0.1))
plt.grid()
plt.title("8x8 Grid")
plt.xlabel("Discount Rate")
plt.ylabel("Time (s)")
plt.savefig("ql_time.png")
plt.close()