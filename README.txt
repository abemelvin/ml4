https://bitbucket.org/abemelvin/ml4/

This repo is split into 2 folders: world_1 and world_2.
world_1 contains all of the scripts related to the 8x8 grid world.
world_2 contains all of the scripts related to the 16x16 grid world.

In each folder, there is an MDP script that corresponds to the world number
(i.e. mdp_1.py, mdp_2.py).
Executing these scripts will generate all of the graph artifacts presented in
the report. 

If you would like to change the "random action rate" and "random action decay rate"
used to manipulate exploration/exploitation, you must manually modify the mdp_class.py
file located in each world's folder. These variables can be found on lines 1060-1061.

Credits to:
https://github.com/sawcordwell/pymdptoolbox
https://stats.stackexchange.com/questions/339592/how-to-get-p-and-r-values-for-a-markov-decision-process-grid-world-problem
for large segments of code to provide a framework for both MDPs and grid worlds.