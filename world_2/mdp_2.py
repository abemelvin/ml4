import numpy as np 
import mdptoolbox
import mdptoolbox.util
import seaborn as sns
import matplotlib.pyplot as plt
from helpers import grid_world_example
from helpers import visualizePolicy
from scipy import linalg
import time
from mdp_class import QLearning

obstacles = [
    (0, 13),
    (1, 2), (1, 5), (1, 13),
    (2, 0), (2, 1), (2, 2), (2, 5), (2, 13),
    (3, 5), (3, 13),
    (4, 5), (4, 10),
    (5, 5), (5, 10),
    (6, 10), (6, 11), (6, 12), (6, 13), (6, 14), (6, 15),
    (15, 0), (7, 1), (7, 2), (7, 3), (7, 4), (7, 5),
    (9, 10),
    (10, 6), (10, 10),
    (11, 6), (11, 10), (11, 14), (11, 15),
    (12, 1), (12, 2), (12, 3), (12, 6), (12, 10),
    (13, 6), (13, 10), (13, 13),
    (14, 6), (14, 13),
    (15, 6), (15, 13)
]

walls = np.zeros((16, 16))
for obstacle in obstacles:
    walls[obstacle[0], obstacle[1]] = 1

P, R = grid_world_example(grid_size=(16, 16), black_cells=obstacles, green_cell_loc=(0, 0), red_cell_loc=(6, 5), start_loc=(15, 0))
mdptoolbox.util.check(P, R)

vi = mdptoolbox.mdp.ValueIteration(P, R, 0.99)
vi.run()
visualizePolicy(vi, (16, 16), walls, start_loc=(15, 0), end_loc=(0, 0), trap_loc=(6, 5), name="correct_policy.png", approach="perfect")
correct_policy = np.reshape(vi.policy, (16, 16))


vi = mdptoolbox.mdp.ValueIteration(P, R, 0.8)
vi.run()
visualizePolicy(vi, (16, 16), walls, start_loc=(15, 0), end_loc=(0, 0), trap_loc=(6, 5), name="vi_policy.png", approach="vi")

pi = mdptoolbox.mdp.PolicyIteration(P, R, 0.7, eval_type=1)
pi.run()
visualizePolicy(pi, (16, 16), walls, start_loc=(15, 0), end_loc=(0, 0), trap_loc=(6, 5), name="pi_policy.png", approach="pi")

np.random.seed(0)
ql = mdptoolbox.mdp.QLearning(P, R, 0.99, 100000)
ql.run()
visualizePolicy(ql, (16, 16), walls, start_loc=(15, 0), end_loc=(0, 0), trap_loc=(6, 5), name="ql_policy.png", approach="ql")


iters_vi = []
accuracy_vi = []
times_vi = []
converged = 1
for rate in np.linspace(0.1, 0.99, 90):
    vi = mdptoolbox.mdp.ValueIteration(P, R, discount=rate)
    start = time.time()
    vi.run()
    times_vi.append(time.time() - start)
    iters_vi.append(vi.iter)
    acc = (np.reshape(vi.policy, (16, 16)) == correct_policy).sum() / 256
    accuracy_vi.append(acc)
    if np.array_equal(np.reshape(vi.policy, (16, 16)), correct_policy) and rate < converged:
        converged = rate

iters_pi = []
accuracy_pi = []
times_pi = []
converged = 1
for rate in np.linspace(0.1, 0.99, 90):
    pi = mdptoolbox.mdp.PolicyIteration(P, R, discount=rate, eval_type=1)
    start = time.time()
    pi.run()
    times_pi.append(time.time() - start)
    iters_pi.append(pi.iter)
    acc = (np.reshape(pi.policy, (16, 16)) == correct_policy).sum() / 256
    accuracy_pi.append(acc)
    if np.array_equal(np.reshape(pi.policy, (16, 16)), correct_policy) and rate < converged:
        converged = rate

plt.plot(np.linspace(0.1, 0.99, 90), iters_vi, label="Value Iteration")
plt.plot(np.linspace(0.1, 0.99, 90), iters_pi, label="Policy Iteration")
plt.legend(loc="best")
plt.xticks(np.arange(0.1, 1.1, 0.1))
#plt.axvline(x=converged, linestyle="--", color="black")
plt.grid()
plt.title("16x16 Grid")
plt.xlabel("Discount Rate")
plt.ylabel("Iterations")
plt.savefig("iterations.png")
plt.close()
plt.plot(np.linspace(0.1, 0.99, 90), accuracy_vi, label="Value Iteration")
plt.plot(np.linspace(0.1, 0.99, 90), accuracy_pi, label="Policy Iteration")
plt.legend(loc="best")
plt.xticks(np.arange(0.1, 1.1, 0.1))
plt.grid()
plt.title("16x16 Grid")
plt.xlabel("Discount Rate")
plt.ylabel("Policy Correctness")
plt.savefig("accuracy.png")
plt.close()
plt.plot(np.linspace(0.1, 0.99, 90), times_vi, label="Value Iteration")
plt.plot(np.linspace(0.1, 0.99, 90), times_pi, label="Policy Iteration")
plt.legend(loc="best")
plt.xticks(np.arange(0.1, 1.1, 0.1))
plt.grid()
plt.title("16x16 Grid")
plt.xlabel("Discount Rate")
plt.ylabel("Time (s)")
plt.savefig("time.png")
plt.close()


accuracy = []
times = []
converged = 1
for rate in np.linspace(0.1, 0.99, 90):
    ql = QLearning(P, R, discount=rate, n_iter=100000)
    start = time.time()
    ql.run()
    times.append(time.time() - start)
    acc = (np.reshape(ql.policy, (16, 16)) == correct_policy).sum() / 256
    accuracy.append(acc)
    if np.array_equal(np.reshape(ql.policy, (16, 16)), correct_policy) and rate < converged:
        converged = rate

plt.plot(np.linspace(0.1, 0.99, 90), accuracy)
plt.xticks(np.arange(0.1, 1.1, 0.1))
plt.grid()
plt.title("16x16 Grid")
plt.xlabel("Discount Rate")
plt.ylabel("Policy Correctness")
plt.savefig("ql_accuracy.png")
plt.close()
plt.plot(np.linspace(0.1, 0.99, 90), times)
plt.xticks(np.arange(0.1, 1.1, 0.1))
plt.grid()
plt.title("16x16 Grid")
plt.xlabel("Discount Rate")
plt.ylabel("Time (s)")
plt.savefig("ql_time.png")
plt.close()